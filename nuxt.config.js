module.exports = {
	modules: [
		'@nuxtjs/axios',
		'@nuxtjs/auth'
	],
	server: {
		port: 3000,
		host: 'zweb.itsboris.net'
	},
	auth: {
		redirect: {
			login: '/login',
			logout: '/login',
			callback: '/projects',
			home: '/'
		},
		rewriteRedirects: true,
		watchLoggedIn: true,
		strategies: {
			local: {
				endpoints: {
					login: {
						url: '/api/login',
						method: 'post',
						propertyName: 'token'
					},
					user: {
						url: '/api/user',
						method: 'get',
						propertyName: 'user'
					},
					logout: {
						url: '/api/logout',
						method: 'post'
					}
				}
			}
		}
	},
	axios: {
		https: true
	},
	css: [
		'@/styles/main.scss'
	],
	router: {
		middleware: ['auth']
	},
	head: {
		title: 'Udimi Test Task on Nuxt',
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: 'Test task made for Upwork proposal for Udimi' },
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
			{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' }
		],
	}
};
