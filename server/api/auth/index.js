const auth = require('./Auth.js');

function writeResponse(response, res, error) {
	if (res) {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(res));
	} else {
		if(error) {
			response.writeHead(error.statusCode, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(error));
		} else {
			response.writeHead(502, {'Content-Type': 'text/plain'});
			response.write(JSON.stringify('Something went wrong'));
		}
	}
	response.end();
}

module.exports = function(app) {
	app.get('/api/user', async (request, response) => {
		let res, error;
		try {
			res = await auth.getUser(request.headers['authorization']);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
	app.post('/api/login', async (request, response) => {
		const data = {
					email: request.body.email,
					password: request.body.password,
				};
		let res, error;
		try {
	 		res = await auth.authorize(data);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
	app.post('/api/logout', async (request, response) => {
		let res, error;
		try {
			res = await auth.logout(request.headers['authorization']);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
};