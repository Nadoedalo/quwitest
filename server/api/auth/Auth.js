const https = require('https');
const requestWrapper = function(options, data) {
	data = data || '';
	data = JSON.stringify(data);
	options.headers = options.headers || {};
	options.headers['Content-Type'] = 'application/json';
	options.headers['Content-Length'] = Buffer.byteLength(data);
	return new Promise(function(resolve, reject) {
		try {
			let request = https.request(options, function(res) {
				let rawData = '';
				res.on('data', function(chunk) {
					rawData += chunk;
				});
				res.on('end', function() {
					if (res.statusCode === 200) {
						let resData = JSON.parse(rawData);
						resolve(
								resData,
						);
					} else {
						reject(
								{
									statusCode: res.statusCode,
									error: rawData,
								},
						);
					}
				});
			});
			request.write(data);
			request.end();
		} catch (e) {
			reject(e);
		}
	});
};
module.exports = {
	getUser: async (token) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'GET',
			path: '/v2/users/profile',
			headers: {
				Authorization : token
			}
		};
		const response = await requestWrapper(options);
		return response;
	},
	authorize: async (data) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'POST',
			path: '/v2/auth/login',
		};
		const response = await requestWrapper(options, data);
		return response;
	},
	logout: async (token) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'POST',
			path: '/v2/auth/logout',
			headers: {
				Authorization : token
			}
		};
		const response = await requestWrapper(options);
		return response;
	},
};