module.exports = function(app, db) {
	var includedAPIs = [ //folder parsing can be added
		'./auth',
		'./projects'
	];
	for(var length = includedAPIs.length, i = 0; i < length; i++){ //initializing APIs
		require(includedAPIs[i])(app, db);
	}
};