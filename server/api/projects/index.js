const projects = require('./Projects.js');

function writeResponse(response, res, error) {
	if (res) {
		response.writeHead(200, {'Content-Type': 'application/json'});
		response.write(JSON.stringify(res));
	} else {
		if(error) {
			response.writeHead(error.statusCode, {'Content-Type': 'application/json'});
			response.write(JSON.stringify(error));
		} else {
			response.writeHead(502, {'Content-Type': 'text/plain'});
			response.write(JSON.stringify('Something went wrong'));
		}
	}
	response.end();
}

module.exports = function(app) {
	app.get('/api/projects', async (request, response) => {
		const token = request.headers['authorization'];
		let res, error;
		try {
			res = await projects.getProjects(token);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
	app.get('/api/project/:id', async (request, response) => {
		const token = request.headers['authorization'];
		const projectID = request.params.id;
		let res, error;
		try {
			res = await projects.getProject(token, projectID);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
	app.post('/api/project/:id', async (request, response) => {
		const token = request.headers['authorization'];
		const projectID = request.params.id;
		const data = request.body;
		let res, error;
		try {
			res = await projects.changeProject(token, projectID, data);
		} catch(e) {
			error = e;
		}
		writeResponse(response, res, error);
	});
};