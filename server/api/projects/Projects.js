const https = require('https');
const requestWrapper = function(options, data) {
	data = data || '';
	data = JSON.stringify(data);
	options.headers = options.headers || {};
	options.headers['Content-Type'] = 'application/json';
	options.headers['Content-Length'] = Buffer.byteLength(data);
	return new Promise(function(resolve, reject) {
		try {
			let request = https.request(options, function(res) {
				let rawData = '';
				res.on('data', function(chunk) {
					rawData += chunk;
				});
				res.on('end', function() {
					if (res.statusCode === 200) {
						let resData = JSON.parse(rawData);
						resolve(
								resData,
						);
					} else {
						reject(
								{
									statusCode: res.statusCode,
									error: rawData,
								},
						);
					}
				});
			});
			request.write(data);
			request.end();
		} catch (e) {
			reject(e);
		}
	});
};
module.exports = {
	getProjects: async (token) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'GET',
			path: '/v2/projects-manage/index', //only owners projects
			headers: {
				Authorization : token
			}
		};
		const response = await requestWrapper(options);
		return response;
	},
	getProject: async (token, projectID) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'GET',
			path: '/v2/projects-manage/'+projectID,
			headers: {
				Authorization : token
			}
		};
		const response = await requestWrapper(options);
		return response;
	},
	changeProject: async (token, projectID, data) => {
		const options = {
			hostname: 'api.quwi.com',
			method: 'POST',
			path: '/v2/projects-manage/update?id='+projectID,
			headers: {
				Authorization : token
			}
		};
		const response = await requestWrapper(options, data);
		return response;
	},
};