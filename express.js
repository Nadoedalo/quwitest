process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // required since certificate is self-signed. Axios doesn't work with those without this flag. FIXME remove from prod environment
const {loadNuxt, build} = require('nuxt');
const isDev = true; //process.env.NODE_ENV !== 'production';
const config = require('./nuxt.config.js');
const mainConfig = require('./server/config/main.js');

(async () => {
	const nuxt = await loadNuxt(isDev ? 'dev' : 'start');
	const express = require('express'),
			app = express(),
			https = require('https'),
			session = require('express-session'),
			fs = require('fs'),
			key = fs.readFileSync('./key.pem'),
			cert = fs.readFileSync('./cert.pem'),
			server = https.createServer({key: key, cert: cert}, app),
			bodyParser = require('body-parser');
	app.use(session({
		secret: 'authToken',
		resave: false,
		saveUninitialized: true
	}));
	app.use(bodyParser.json());
	require('./server/api')(app);

	build(nuxt).then(() => {
		app.use(nuxt.render);
		server.listen(mainConfig.port, function() {
			console.log('We are live on ' + mainConfig.port);
		});
	});
})();
